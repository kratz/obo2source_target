# Convert from OBO to source-target format

File in OBO format [1] needs to be converted to "source-target" format. A simple short Perl script to do this.

```bash
wget http://snapshot.geneontology.org/ontology/go-basic.obo
```
 
### References
[1] The OBO Flat File Format Guide, version 1.4 https://owlcollab.github.io/oboformat/doc/GO.format.obo-1_4.html

