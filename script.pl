# author:	Anton Kratz 
# created:	Tue Jul 10 13:43:02 PDT 2018

# parse a gene ontoilogy *.obo - file

use strict;
my $in;
my @paragraph;
my $k;

# go into paragraph mode
local $/ = "";

open FH, "go-basic.obo" or die $!;
while ($in = <FH>) {
	chomp $in;
	@paragraph = split /\n/, $in;
	print $paragraph[1] . "\n";
	foreach $k (@paragraph) {
		# if $k starts with the string "is_a"
		if (index($k, "is_a") == 0) {
			print "\t$k\n";
		}
	}
}
close FH;
